package models;

import org.junit.Test;

import static models.User.OcaStatus;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

/**
 * Basic User persistence tests.
 * TODO: Tidy tests so they have one assert each
 */
public class UserTest extends AbstractPersistenceIntegrationTest {

    private static final String TEST_BITBUCKET_USERNAME = "danielbryant_uk";
    private static final String TEST_OPENJDK_USERNAME = "danielbryant_ojdk";
    private static final String TEST_NAME = "Daniel Bryant";
    private static final OcaStatus TEST_OCA_SIGNED = OcaStatus.SIGNED;
    private static final User.OcaStatus TEST_OCA_SIGNED = User.OcaStatus.SIGNED;
    private static final User.OcaStatus TEST_OCA_UNKNOWN = User.OcaStatus.UNKNOWN;

    public static User createTestInstance() {
        return createTestInstance(OcaStatus.UNKNOWN);
    }

    private static User createTestInstance(OcaStatus status) {
        User user = new User(TEST_BITBUCKET_USERNAME, TEST_NAME, status);
        user.openjdkUsername = TEST_OPENJDK_USERNAME;
        return user;
    }
    
	public static User createTestInstanceWithRequesterOcaSignedUnknow() {
        User user = new User(TEST_BITBUCKET_USERNAME, TEST_NAME, TEST_OCA_UNKNOWN);
        user.openjdkUsername = TEST_OPENJDK_USERNAME;
        return user;
	}
	
    @Test
    public void save_validUser_userPersisted() {
        User user = createTestInstance(OcaStatus.SIGNED);
        assertThat(user.id, is(nullValue()));

        user.save();

        assertThat(user.id, is(not(nullValue())));
        assertThat(user.createdDate, is(notNullValue()));
    }

    @Test
    public void save_validUser_userPersistedWithNoOcaValue() {
        User user = createTestInstance(null);
        assertThat(user.id, is(nullValue()));

        user.save();

        assertThat(user.id, is(not(nullValue())));
        assertThat(user.createdDate, is(notNullValue()));
        assertThat(user.ocaStatus, is(notNullValue()));
    }

    @Test
    public void save_validUser_userPersistedWithNotSignedOcaValue() {
        User user = createTestInstance(OcaStatus.NOT_SIGNED);
        assertThat(user.id, is(nullValue()));

        user.save();

        assertThat(user.id, is(not(nullValue())));
        assertThat(user.createdDate, is(notNullValue()));
        assertThat(user.ocaStatus.displayName(), is("not signed"));
    }
}