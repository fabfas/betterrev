package utils;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

/**
 * The unit tests below are using the Given..When..Then style,
 * and hence each step (block of code) is separated by a blank line.
 *
 */
public final class FetchDiffFilesStringTest {
    @Test
    public void shouldReturnDiffFilesStringForPullRequestURL() {
        String pullRequestForOwnerUrl =
                String.format("https://bitbucket.org/api/2.0/repositories/%s/%s/pullrequests/%s/diff/",
                    "AdoptOpenJDK","better-test-repo", "1");
        String expectedDiffFilesString = "diff -r 6e9df89ba172268db2ff815caee67cc1c70298b1 -r 218c7ad8deb066b652970cb69bd8256cd7827f44 new_file\n" +
                "--- /dev/null\n" +
                "+++ b/new_file\n" +
                "@@ -0,0 +1,1 @@\n" +
                "+some new code\n";

        String actualDiffFilesString = FetchDiffFilesString.from(pullRequestForOwnerUrl);

        assertThat(actualDiffFilesString, is(equalTo(expectedDiffFilesString)));
    }
}
