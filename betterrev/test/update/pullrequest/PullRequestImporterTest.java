package update.pullrequest;

import akka.actor.ActorRef;
import models.Contribution;
import models.ContributionEvent;
import models.User.OcaStatus;

import org.codehaus.jackson.JsonNode;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;

import java.util.Collection;
import java.util.List;

import static java.util.concurrent.TimeUnit.SECONDS;
import static models.ContributionEventType.CONTRIBUTION_MODIFIED;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class PullRequestImporterTest extends AbstractPullRequestImporterTest {

    private static final String KARIANNA_CREATION = "2013-11-09T12:27:10.557977+00:00";

    @BeforeClass
    public static void setupActors() {
        ActorRef pullRequestImporterActor = actorRule.actorOf(PullRequestImporter.class);
        eventStream.subscribe(pullRequestImporterActor, ImportPullRequestsEvent.class);
    }

    @Before
    public void dropEntries() {
        for (Contribution contribution : Contribution.find.all())
            contribution.delete();
    }

    @Test
    public void importsNewPullRequest() throws Exception {
        PullRequestsImportedEvent reply = importPullRequests();

        Contribution contribution = reply.getContributions().get(0);
        String name = "Change to Readme purely to create a new PR";
        String description = "New test PR";
        checkContributionDataWasUpdated(contribution, name, description, "2013-11-09T12:27:10.586683+00:00", "2", KARIANNA_CREATION);
        assertContributionEquals(contribution, repositoryId, "2", name, description);
        assertRequesterInfoEquals(contribution, "karianna", "Martijn Verburg");
    }

    @Test
    public void importAllPullRequests_validRepo_fullUrlIsBuiltSuccessfully() throws Exception {
        PullRequestsImportedEvent reply = importPullRequests();

        List<Contribution> contributions = reply.getContributions();
        Contribution contribution = contributions.get(0);
        assertThat(contribution.pullRequestUrl(),
                   is("https://bitbucket.org/api/2.0/repositories/karianna/better-test-repo/pullrequests/2/diff"));

        contribution = contributions.get(1);
        assertThat(contribution.pullRequestUrl(),
                   is("https://bitbucket.org/api/2.0/repositories/richardwarburton/better-test-repo/pullrequests/1/diff"));
    }

    @Test
    public void importIsComingFromDefaultBranch() throws Exception {
        PullRequestsImportedEvent reply = importPullRequests();
        Contribution contribution = reply.getContributions().get(0);
        assertEquals("default", contribution.branchName);
        assertTrue(contribution.isDefaultBranch());
    }

    @Test
    public void importIsntComingFromDefaultBranch() throws Exception {
        PullRequestsImportedEvent reply = importPullRequests();
        Contribution contribution = reply.getContributions().get(1);
        assertEquals("test_branch", contribution.branchName);
        assertFalse(contribution.isDefaultBranch());
    }

    @Test
    public void updatesContributions() throws Exception {
        // Load the old version of the Contribution, so there's something to update
        Contribution oldContribution = performFirstContributionAction();
        Contribution newContribution = performSecondContributionAction();

        assertEquals(oldContribution.id, newContribution.id);

        assertTrue("Could not find Modified ContributionEventType",
                   newContribution.hasContributionEventWith(CONTRIBUTION_MODIFIED));

        String name = "Change to Readme purely to create a new PR - Updated";
        String description = "New test PR - Updated";
        checkContributionDataWasUpdated(newContribution, name, description, "2013-11-09T12:55:41.177558+00:00", "2", KARIANNA_CREATION);
    }

    @Test
    public void shouldApplyExternalPullRequestURLToContributionEventWhenContributionIsUpdated() throws Exception {
        // https://bitbucket.org/api/2.0/repositories/adoptopenjdk/better-test-repo/pullrequests/1 -> { links: self:'<link>'}
        String linkToExternalInfo = "https://bitbucket.org/api/2.0/repositories/adoptopenjdk/better-test-repo/pullrequests/2";

        // Load the old version of the Contribution, so there's something to update
        Contribution oldContribution = performFirstContributionAction();
        Contribution newContribution = performSecondContributionAction();
        Collection<ContributionEvent> contributionEvents =
                newContribution.getContributionEventWithType(CONTRIBUTION_MODIFIED);
        ContributionEvent contributionEvent = null;
        if (contributionEvents.iterator().hasNext()) {
            contributionEvent = contributionEvents.iterator().next();
        }

        assertThat(contributionEvent.linkToExternalInfo, is(linkToExternalInfo));
    }
    
    @Test
    public void checkOcaStatusPullRequest() throws Exception {
        PullRequestsImportedEvent reply = importPullRequests();

        Contribution contribution = reply.getContributions().get(0);
        assertEquals(OcaStatus.SIGNED,contribution.requester.ocaStatus);
        Contribution contribution2 = reply.getContributions().get(1);
        assertEquals(OcaStatus.SIGNED,contribution2.requester.ocaStatus);
    }
    
    private Contribution performFirstContributionAction() throws Exception {
        return performPullRequestAction(firstResponse);
    }

    private Contribution performSecondContributionAction() throws Exception {
        return performPullRequestAction(secondResponse);
    }

    private Contribution performPullRequestAction(JsonNode sampleResponse) throws Exception {
        Future<PullRequestsImportedEvent> response =
                actorRule.expectMsg(PullRequestsImportedEvent.class, SECONDS.toMillis(5));
        eventStream.publish(new ImportPullRequestsEvent(sampleResponse, repositoryId));
        return Await.result(response, Duration.Inf()).getContributions().get(0);
    }

    private PullRequestsImportedEvent importPullRequests() throws Exception {
        Future<PullRequestsImportedEvent> response =
                actorRule.expectMsg(PullRequestsImportedEvent.class, SECONDS.toMillis(5));
        eventStream.publish(new ImportPullRequestsEvent(firstResponse, repositoryId));
        PullRequestsImportedEvent reply = Await.result(response, Duration.Inf());
        return reply;
    }

    private static void checkContributionDataWasUpdated(Contribution contribution, String name,
                                                        String description, String updatedAt, String id, String createdAt) {
        assertContributionEquals(contribution, repositoryId, id, name, description);
        assertTimePointsEquals(contribution, DateTime.parse(createdAt), DateTime.parse(updatedAt));
    }

    private static void assertTimePointsEquals(Contribution contribution, DateTime expectedCreated, DateTime expectedUpdated) {
        assertEquals("Wrong creation timepoint", expectedCreated.getMillis(), contribution.createdOn.getMillis());
        assertEquals("Wrong update timepoint", expectedUpdated.getMillis(), contribution.updatedOn.getMillis());
    }

    private static void assertContributionEquals(Contribution contribution, String repoId, String pullRequestId,
                                                 String name, String description) {
        assertEquals(repoId, contribution.repositoryId);
        assertEquals(pullRequestId, contribution.pullRequestId);
        assertEquals(name, contribution.name);
        assertEquals(description, contribution.description);
    }

    private static void assertRequesterInfoEquals(Contribution contribution, String bitbucketName, String displayName) {
        assertEquals(bitbucketName, contribution.requester.bitbucketUserName);
        assertEquals(displayName, contribution.requester.name);
    }
}