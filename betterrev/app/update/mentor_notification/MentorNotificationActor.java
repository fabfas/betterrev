package update.mentor_notification;

import models.ContributionEvent;
import models.ContributionEventType;
import update.BetterrevActor;

import static models.ContributionEventType.MENTOR_NOTIFIED;

/**
 * MentorNotificationActor
 */
public class MentorNotificationActor extends BetterrevActor {

    private EmailHelper emailHelper = new EmailHelper();

    @Override
    public void onReceive(Object message) throws Exception {
        if (!(message instanceof ContributionEvent)) {
            return;
        }
        ContributionEvent request = (ContributionEvent) message;
        if (request.contributionEventType != ContributionEventType.OCA_SIGNED) {
            return;
        }

        emailHelper.sendEmailMessage(request);
        publishMentorNotifiedEvent(request);
    }

    private void publishMentorNotifiedEvent(ContributionEvent request) {
        ContributionEvent mentorNotifiedEvent = new ContributionEvent(MENTOR_NOTIFIED, request.contribution);
        eventStream().publish(mentorNotifiedEvent);
    }


}
