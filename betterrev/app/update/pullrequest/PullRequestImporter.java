package update.pullrequest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import models.Contribution;
import models.ContributionEvent;
import models.ContributionEventType;
import models.State;
import models.User;

import org.codehaus.jackson.JsonNode;
import org.joda.time.DateTime;

import play.Logger;
import update.BetterrevActor;

/**
 * This Class is responsible for importing BitBucket pullrequests into the Betterrev application.
 */
public final class PullRequestImporter extends BetterrevActor {

    @Override
    public void onReceive(Object message) throws Exception {
        if (message instanceof ImportPullRequestsEvent) {
            Logger.debug("ImportPullRequestsEvent received.");

            ImportPullRequestsEvent importRequest = (ImportPullRequestsEvent) message;
            List<Contribution> contributions = convertJsonIntoContributions(importRequest.getJsonNode(), importRequest.getProject());
            eventStream().publish(new PullRequestsImportedEvent(contributions));
        }
    }

    public List<Contribution> convertJsonIntoContributions(JsonNode response, String repositoryId) {
        JsonNode values = response.get("values");
        Iterator<JsonNode> elements = values.getElements();
        List<Contribution> contributions = new ArrayList<>();
        while (elements.hasNext()) {
            Contribution contribution = convertJsonNodeIntoContribution(elements.next(), repositoryId);
            contributions.add(contribution);
        }
        return contributions;
    }

    private Contribution convertJsonNodeIntoContribution(JsonNode requestNode, String repositoryId) {
        String requestId = requestNode.get("id").asText();
        Contribution contribution = Contribution.findByBitbucketIds(repositoryId, requestId);
        DateTime updatedOn = getDateTime(requestNode, "updated_on");
        if (contribution == null) {
            return createContribution(requestNode, repositoryId, requestId, updatedOn);
        } else {
            ensureContributionUpdated(requestNode, updatedOn, contribution);
            return contribution;
        }
    }

    private Contribution ensureContributionUpdated(JsonNode requestNode, DateTime updatedOn, Contribution contribution) {
        if (!contribution.wasUpdatedBefore(updatedOn)) {
            return contribution;
        }

        contribution.updatedOn = updatedOn;
        contribution.name = getTitle(requestNode);
        contribution.description = getDescription(requestNode);

        String linkToExternalInfo = getLinkToExternalInfo(requestNode);
        return updateContribution(contribution, linkToExternalInfo);
    }

    private Contribution updateContribution(Contribution contribution, String linkToExternalInfo) {
        if (contribution.state != State.OPEN) {
            throw new IllegalStateException("Cannot transition from current State of " + contribution.state);
        }

        contribution.contributionEvents.add(
                new ContributionEvent(ContributionEventType.CONTRIBUTION_MODIFIED, linkToExternalInfo));
        contribution.state = State.OPEN;
        contribution.update();

        return contribution;
    }

    private Contribution createContribution(JsonNode requestNode, String repositoryId, String requestId, DateTime updatedOn) {
        JsonNode userNode = requestNode.get("author");
        String bitbucketUserName = userNode.get("username").asText();
        String displayName = userNode.get("display_name").asText();
        User user = User.findOrCreate(bitbucketUserName, displayName);   

        DateTime createdOn = getDateTime(requestNode, "created_on");
        String branchName = getBranchNameFrom(requestNode);
        Contribution contribution = new Contribution(repositoryId,
                                                     requestId,
                                                     getTitle(requestNode),
                                                     getDescription(requestNode),
                                                     user,
                                                     createdOn,
                                                     updatedOn,
                                                     branchName);
        contribution.save();
        Logger.debug(String.format("Created new Contribution - id: %s, bitbucket username: %s", requestId, user.bitbucketUserName));
        publishContributionGeneratedEvent(contribution, "");
        return contribution;
    }

    private String getBranchNameFrom(JsonNode requestNode) {
        return requestNode.get("destination")
                .get("branch")
                .get("name")
                .asText();
    }

    private void publishContributionGeneratedEvent(Contribution contribution, String linkToExternalInfo) {
        if (contribution.state != State.NULL) {
            throw new IllegalStateException("Cannot transition from current State of " + contribution.state);
        }

        ContributionEvent contributionGeneratedEvent = new ContributionEvent(
                ContributionEventType.CONTRIBUTION_GENERATED, linkToExternalInfo
        );

        contribution.contributionEvents.add(contributionGeneratedEvent);

        contribution.state = State.OPEN;
        contribution.update();

        eventStream().publish(contributionGeneratedEvent);
    }

    private static DateTime getDateTime(JsonNode requestNode, String field) {
        String dateString = requestNode.get(field)
                .asText()
                .replace(' ', 'T');
        return DateTime.parse(dateString);
    }

    private static String getDescription(JsonNode requestNode) {
        return requestNode.get("description").asText();
    }

    private static String getTitle(JsonNode requestNode) {
        return requestNode.get("title").asText();
    }

    private String getLinkToExternalInfo(JsonNode requestNode) {
        return requestNode.get("links")
                .get("self")
                .get("href")
                .asText();
    }
}