package utils.exception;

public class OCAComunicationException extends Exception {

	private static final long serialVersionUID = 7315526517636583172L;

	public OCAComunicationException(String message, Throwable e) {
		 super(message,e);
	}

}
